import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");

        Phonebook myPhonebook = new Phonebook();
            // Contact 1
        Contact john = new Contact();
        john.setName("John Doe");
        john.setContactNumber("+639152468596");
        john.setAddress("my home in Quezon City");

            // contact 2
//        Contact jane = new Contact();
//        jane.setName("Jane Doe");
//        jane.setContactNumber("+639162148573");
//        jane.setAddress("my home in Caloocan City");

        Contact jane = new Contact("Jane Doe", "+639162148573", "my home in Caloocan City");

            // setting contacts
        myPhonebook.setContacts(john);
        myPhonebook.setContacts(jane);

            // display

        if(myPhonebook.getContacts().isEmpty()){
            System.out.println("The phonebook is empty");
        } else {
            myPhonebook.getContacts().forEach(contact -> myPhonebook.displayPhoneBook(contact));
        }

    }
}