package com.zuitt.example;

import java.util.ArrayList;

public class Contact {
    // properties
    private String name;
    private String contactNumber;
    private String address;

    // constructors

    // default constructor
    public Contact(){};

    // Parameterized constructor
    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber= contactNumber;
        this.address= address;

    }

    // Getters and Setters

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getContactNumber(){
        return contactNumber;
    }

    public void setContactNumber(String contactNumber){
        this.contactNumber= contactNumber;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String address){
        this.address = address;
    }


}
