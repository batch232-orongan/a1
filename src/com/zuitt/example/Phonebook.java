package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook{
    // properties
    private ArrayList<Contact> contacts = new ArrayList<>();

    // constructors
    // default constructor
    public Phonebook(){};

    // parameterized constructor
    public Phonebook (Contact contact){
        this.contacts.add(contact);
    }

    // getters and setters
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Contact contact){
        this.contacts.add(contact);
    }

    // method
    public void displayPhoneBook(Contact contact){
        System.out.println(contact.getName());
        System.out.println("---------------");
        if(contact.getContactNumber().isEmpty()){
            System.out.println(contact.getName() + " has no registered number.");
        } else{
            System.out.println(contact.getName() + " has the following registered number.");
            System.out.println(contact.getContactNumber());
        }
        if(contact.getAddress().isEmpty()){
            System.out.println(contact.getName() + "has no registered address.");
        } else{
            System.out.println(contact.getName() + " has the following registered address.");
            System.out.println(contact.getAddress());
        }







    }







}
